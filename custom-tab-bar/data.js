export default [
  {
    icon: 'location',
    text: '附近',
    url: 'pages/home/home',
  },
  {
    icon: 'home',
    text: '茶馆',
    url: 'pages/goods/details/index',
  },
  {
    icon: 'bulletpoint',
    text: '订单',
    url: 'pages/cart/index',
  },
  {
    icon: 'user-circle',
    text: '我',
    url: 'pages/usercenter/index',
  },
];
